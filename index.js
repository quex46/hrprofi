const bluebird = require('bluebird');
const mongoose = require('./src/lib/mongoose');
const config = require('./config');
const server = require('./src/server');

function createAdminAccount() {
  const User = mongoose.model('User');

  return bluebird
    .resolve(User.findOne({ 
      username: config.admin_username,
    }))
    .then((user) => {
      if (!user) {
        user = new User();
      }

      user.set({
        username: config.admin_username,
        password: config.admin_password
      });
      
      return user.save();
    });
}

bluebird
  .fromCallback((cb) => mongoose.connect(config.mongo, cb))
  .then(createAdminAccount)
  .then(() => server.listen(config.server_port));

// Иначе докер не реагирует на Ctrl+C
process.on('SIGTERM', () => process.exit()); 
process.on('SIGINT', () => process.exit());

