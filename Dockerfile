FROM node

RUN apt-get update \
&& apt-get install \
libpng-dev \
libjpeg-dev \
libjasper-dev

RUN cd /tmp \
&& wget ftp://ftp.icm.edu.pl/pub/unix/graphics/GraphicsMagick/GraphicsMagick-LATEST.tar.xz \
&& tar -xJf GraphicsMagick-LATEST.tar.xz \
&& cd GraphicsMagick* \
&& ./configure \
&& make \
&& make install \
&& rm -rf /tmp/Graphics*

COPY package.json index.js config.json /var/www/
COPY src /var/www/src/

RUN cd /var/www && npm install

CMD ["node", "/var/www/index.js"]