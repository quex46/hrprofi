$.widget('app.feed', {
  options: {
    template: '<div></div>',
    limit: 10
  },

  _create: function () {
    this.template = _.template(_.unescape(this.options.template));
    this._on({
      'click .js-nav-btn': function () {
        var ts = this._getMinTimestamp();
        this.load(-ts);
      }
    });
    this.load();
  },

  _render: function (data) {
    var d = $.extend({}, data, {
      created_human: new Date(data.created).toLocaleString()
    });
    return this.template(d);
  },

  _getMinTimestamp: function () {
    var minTs = null;
    var childs = this.element.find('.js-feed-container').children();
    
    $.each(childs, function (idx, child) {
      var childTs = +$(child).attr('data-timestamp');
      if (minTs === null || childTs < minTs) {
        minTs = childTs;
      }
    });

    return minTs;
  },

  push: function (data) {
    var cont = this.element.find('.js-feed-container');
    var childs = cont.children();
    
    cont.prepend(this._render(data));
    
    if (childs.length > this.options.limit) {
      childs[this.options.limit].remove();
    }
  },

  load: function (timestamp) {
    var ts = +timestamp || 0;
    var self = this;
    
    $.ajax({
      method: 'GET',
      url: '/admin/feed',
      global: false,
      dataType: 'json',
      data: {
        timestamp: ts,
        limit: this.options.limit
      },
      success: function (json) {
        var out = '';        
        var btn = self.element.find('.js-nav-btn');

        btn.show();
        
        if (!json.length || json.length < self.options.limit) {
          btn.hide();
        }
        
        $.each(json, function (key, data) {
          out += self._render(data);
        });

        if (out) {
          self.element.find('.js-feed-container').html(out);
        }        
      }
    });
  }
});