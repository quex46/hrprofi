const mongoose = require('mongoose');
const bluebird = require('bluebird');
const path = require('path');
const mkdirp = require('mkdirp');
const md5file = require('md5-file');
const gm = require('gm');
const _ = require('underscore');
const fs = require('fs');

const Message = new mongoose.Schema({
  title: String,
  message: String,
  image: String,
  created: Number
}, { versionKey: false, id: false });

/*================================
=            Свойства            =
================================*/

/**
 * Метка создания сообщения
 */
Message.path('created')
  .default(() => Date.now());

Message.virtual('image_full')
  .get(function () {
    return this.image || '';
  });

Message.virtual('image_medium')
  .get(function () {
    if (this.image) {
      return this.image.replace('_full.', '_medium.');
    } else {
      return '';
    }
  });

Message.virtual('image_small')
  .get(function () {
    if (this.image) {
      return this.image.replace('_full.', '_small.');
    } else {
      return '';
    }
  });

Message.path('title')
  .set((v) => v.trim())
  .validate((v) => v.length > 0, 'Не указан заголовок')
  .validate((v) => v.length < 255, 'Заголовок слишком длинный');

Message.path('message')
  .set((v) => v.trim())
  .validate((v) => v.length > 0, 'Не указан текст сообщения')
  .validate((v) => v.length < 4000, 'Текст сообщения слишом длинный');

/*==========================================
=            Статические методы            =
==========================================*/

/**
 * Сохранение загруженной картинки
 * @param  {string}       imgPath       Путь до временного файла
 * @return {array}                      Пути до сохраненных изображений
 */
Message.statics.handleUploadedImage = function (imgPath) {
  return bluebird
    .promisify(md5file)(imgPath)
    .then(function (hash) {
      var dir = path.join(
        __dirname, '../public/img/upload', hash.substr(0, 2), hash.substr(2, 2)
      );

      return bluebird.promisify(mkdirp)(dir).return(dir + '/' + hash);
    })
    .then((basepath) => {
      var save = (width, height, quality, suffix) => {
        var out = basepath + '_' + suffix + '.png';
        
        return bluebird
          .fromCallback((cb) => {
            return gm(imgPath)
              .resize(width, height, '>')
              .quality(quality)
              .write(out, cb);
          })
          .return(out);
      };

      return bluebird.all([
        save(6000, 6000, 100, 'full'),
        save(800, 800, 90, 'medium'),
        save(200, 200, 100, 'small'),
      ]);
    })
    .tap(() => bluebird.promisify(fs.unlink)(imgPath));
};

/**
 * Обработка пользовательского запроса на создание сообщения
 * @param  {object} body Тело запроса (this.request.body)
 * @return {Message}     Результат
 */
Message.statics.handleNewMessageRequestBody = function (body) {
  var data = _.pick(body.fields, ['title', 'message']);
  var image = body.files.image;

  return bluebird
    .resolve(new this(data))
    .then((message) => {
      if (image.size === 0) {
        return message;
      }

      return this
        .handleUploadedImage(image.path)
        .then((paths) => {
          var root = path.join(__dirname, '../public');
          
          message.image = '/' + path.relative(root, paths[0]);
          
          return message;
        });
    })
    .then((message) => message.save())
    // Такой вариант будет работать только для 1 инстанса,
    // в случае масштабируемого приложения следует использовать 
    // общий буфер (например, Redis).
    .tap((message) => require('../server').io.emit('new message', message.toFeedItem()))
    .catch((err) => {
      var errors = [];
      
      switch (err.name) {
        case 'ValidationError':
          errors = _.reduce(err.errors, (ret, err) => {
            ret.push(err.message);
            return ret;
          }, []);          
          break;
        
        default:
          errors.push('Неизвестная ошибка. Попробуйте еще раз.');
          break;
      }

      throw errors;
    });
};

/**
 * Лента сообщений
 * @param  {object} options Параметры запроса
 * @return {array}          Массив сообщений
 */
Message.statics.feed = function (options) {
  var opts = {};

  if (_.isObject(options)) {
    opts = _.pick(options, ['timestamp', 'limit']);
  }
  
  opts.timestamp = +opts.timestamp || 0;
  opts.limit = Math.min(+opts.limit || 10, 100);

  var query;

  if (opts.timestamp >= 0) {
    query = { created: { $gt: opts.timestamp } };
  } else {
    query = { created: { $lt: Math.abs(opts.timestamp) } };
  }

  var queryOpts = { limit: opts.limit, sort: '-created' };

  return this
    .find(query, null, queryOpts)
    .then((docs) => docs.map((doc) => doc.toFeedItem()));
};

/*==============================
=            Методы            =
==============================*/

Message.methods.toFeedItem = function () {
  return this.toObject({
    getters: true,
    transform: (doc, ret) => _.pick(ret, [
      'image_full',
      'image_medium',
      'image_small',
      'title',
      'message',
      'created'
    ])
  });
};


module.exports = mongoose.model('Message', Message, 'messages');
