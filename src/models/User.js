const mongoose = require('mongoose');
const bluebird = require('bluebird');
const crypto = require('crypto');

const User = new mongoose.Schema({
  username: String,
  password: String,
  salt: String
}, { versionKey: false, id: false });

/*================================
=            Свойства            =
================================*/

User.path('username')
  .required(true)
  .unique(true);

User.path('password')
  .required(true)  
  .set(function (pass) {
    // В базе храним соленый хэш пароля.
    return this.constructor.getPasswordHash(pass, this.salt);
  });

/**
 * Соль для подмешивания в хэш пароля
 */
User.path('salt')
  .default(() => crypto.randomBytes(16).toString('hex'));

/*==============================
=            Методы            =
==============================*/

/**
 * Проверка пароля пользователя
 * @param  {string}   pass    Пароль для проверки
 * @return {bool}             Результат проверки пароля
 */
User.methods.validatePassword = function (pass) {
  return this.password === this.constructor.getPasswordHash(pass, this.salt);
};

/**
 * Установка сессионных кук
 * @param {Object}  cookies   Объект cookies из koa (this.cookies)
 */
User.methods.setSessionCookies = function (cookies) {
  cookies.set('uid', this._id);
  cookies.set('password', this.password);
};

/*==========================================
=            Статические методы            =
==========================================*/

/**
 * Формирует хэш пароля, для сохранения в базе
 * @param  {string} pass Пароль
 * @param  {string} salt Соль для подмешивания в хэш
 * @return {string}      Соленый хэш пароля
 */
User.statics.getPasswordHash = function (pass, salt) {
  return crypto
    .createHash('sha256')
    .update( (salt || '') + ':' + (pass || '') )
    .digest('hex');
};

/**
 * Поиск пользователя по сессионным кукам
 * @param  {object}   cookies   Объект cookies из koa (this.cookies)
 * @return {User|null}     
 */
User.statics.findByCookies = function (cookies) {
  var args = Array.prototype.slice.call(arguments);
  
  args[0] = {
    _id: cookies.get('uid'),
    password: cookies.get('password')
  };

  return this.findOne.apply(this, args);
};

/**
 * Очистка сессионных кук
 * @param  {object}   cookies   Объект cookies из koa (this.cookies)
 */
User.statics.destroySessionCookies = function (cookies) {
  cookies.set('uid', '');
  cookies.set('password', '');
};

/**
 * Обработка запроса на авторизацию
 * @param  {object} body this.request.body из koa
 * @return {User}        Пользователь подходяший под учетные данные
 */
User.statics.handleLoginRequest = function (body) {
  var username = body.username;
  var password = body.password;

  return bluebird
    .delay(1000) // Снижаем эффективность брутфорса
    .then(() => this.findOne({ username }, '_id salt password'))
    .tap((user) => {  
      if (!user || !user.validatePassword(password)) {
        var err = new Error('Пользователь не найден');
        err.name = 'UnknownUser';
        throw err;
      }
    })
    .catch((err) => {
      var errors = [];
      switch (err.name) {
        case 'UnknownUser':
          errors.push(err.message);
          break;
        default:
          console.log(err);
          errors.push('В процессе авторизаци произошла ошибка. Попробуйте еще раз');
          break;
      }
      throw errors;
    });
};

module.exports = mongoose.model('User', User, 'users');