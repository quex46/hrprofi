const _ = require('underscore');
const bluebird = require('bluebird');
const mongoose = require('mongoose');

const models = [
  'User',
  'Message'
];

mongoose.Promise = bluebird;

_.each(models, (name) => require('../models/' + name));

module.exports = mongoose;