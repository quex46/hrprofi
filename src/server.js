const app = require('koa')();
const router = require('./router');
const assets = require('koa-static');

/**
 * Рендеринг шаблонов.
 */

const nunjucks = require('nunjucks');
const _ = require('underscore');

(function () {
  let viewsDir = __dirname + '/views';
  let noCache = ['production', 'staging'].indexOf(process.env.NODE_ENV) === -1;
  let loader = new nunjucks.FileSystemLoader(viewsDir, { noCache });
  var njEnv = new nunjucks.Environment(loader);

  app.context.render = function render(templateName, _locals) {
    let locals = _.extend(this.state, _locals);
    
    this.type = 'html';
    this.body = njEnv.render(templateName, locals);
  };
})();

app.use(assets(__dirname + '/public'));
app.use(router.routes());

const server = require('http').Server(app.callback());
server.io = require('socket.io')(server);

module.exports = server;