const router = module.exports = require('koa-router')();
const body = require('koa-body')({ multipart: true });
const _ = require('underscore');
const mongoose = require('./lib/mongoose');
const User = mongoose.model('User');
const Message = mongoose.model('Message');

// Инициализация сессии
router.use(function* session(next) {
  this.state.user = yield User.findByCookies(this.cookies);
  yield next;
});

// Проверка авторизации при доступе к разделам админки
router.use('/admin', function* (next) {
  if (this.state.user || this.request.path === '/admin/login') {
    yield next;
    return;
  }

  this.redirect('/admin/login');
});

router.get('/', function* () {
  if (this.request.query.ok) {
    this.render('upload_success.html');
  } else {
    this.render('upload.html');
  }  
});

router.post('/', body, function* () {
  try {
    yield Message.handleNewMessageRequestBody(this.request.body);
    this.redirect('/?ok=1');
  } catch (errors) {
    this.render('upload.html', {
      errors,
      message: _.pick(this.request.body.fields, ['title', 'message'])
    });
  }
});

router.get('/admin', function* () {
  this.render('admin/index.html');
});

router.get('/admin/feed', function* () {
  this.type = 'json';
  try {    
    this.body = yield Message.feed(this.request.query);
  } catch (e) {
    this.body = [];
  }
});

router.get('/admin/login', function* () {
  this.render('admin/login.html');
});

router.post('/admin/login', body, function* () {
  try {
    var user = yield User.handleLoginRequest(this.request.body);
    user.setSessionCookies(this.cookies);
    this.redirect('/admin');
  } catch (errors) {
    this.render('admin/login.html', { errors });
  }
});

router.get('/admin/logout', function* () {
  this.render('admin/logout.html');
});

router.post('/admin/logout', function* () {
  User.destroySessionCookies(this.cookies);
  this.redirect('/admin/login');
});